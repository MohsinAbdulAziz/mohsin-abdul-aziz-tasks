const jsonfile = require("jsonfile");
const randomstring = require("randomstring");
 
const inputFile = "input2.json";
const outputFile = "output2.json";


var output = {}

  jsonfile.readFile(inputFile, function(err, body){
    console.log("Loading names from input2.json");
    output.emails= [];
    for (var names in body) {
        var names = body[names];
        for (var name in names) {
            if(names.hasOwnProperty(name)){
                var originalName=names[name] //Each name is sorted to perform reverse function
                var reverseName= originalName.split("").reverse().join(""); //Applying name reverse function
                output.emails.push(reverseName.concat(randomstring.generate(5)).concat("@gmail.com")) //Adding 5 random character and @gmail.com                    
              }
           
        }
     }
     jsonfile.writeFile(outputFile, output, {spaces: 2}, function(err) {
        console.log("All done!");
      });
 
})